//-------------------------------------------------------------------------
/* 2: Implementation Using Flow Library (Async.js) */
//-------------------------------------------------------------------------
const cheerio = require("cheerio");
const express = require("express");
const axios = require("axios");
const async = require("async");
const app = express();
const port = 3000;
//-------------------------------------------------------------------------
// Setting the view engine to 'ejs'
//-------------------------------------------------------------------------
app.set("view engine", "ejs");
//-------------------------------------------------------------------------
// Handling required single 'GET' request route
//-------------------------------------------------------------------------
app.get("/I/want/title/:address?", (req, res) => {
    let titles = [];
    let addresses = parseAddress(req.query.address);

    if (addresses) {
        async.each(
            addresses,
            (url, callback) => {
                let parsedUrl = parseUrl(url);
                if (typeof parsedUrl == "string") {
                    axios
                        .get(parsedUrl)
                        .then(response => {
                            const $ = cheerio.load(response.data);
                            titles.push({
                                address: url,
                                title: $("title").text()
                            });
                            callback();
                        })
                        .catch(error => {
                            console.log(error);
                        });
                } else {
                    titles.push({
                        address: url,
                        title: "No Response"
                    });
                    callback();
                }
            },
            err => {
                if (err) {
                    console.log("A url failed to process");
                } else {
                    res.render("index", {
                        titles: titles
                    });
                }
            }
        );
    } else {
        res.render("index", {
            titles: titles
        });
    }
});
//-------------------------------------------------------------------------
// Handling all other routes
//-------------------------------------------------------------------------
app.get("*", (req, res) => {
    res.render("404");
});
//-------------------------------------------------------------------------
// To parse address from url based on type
//-------------------------------------------------------------------------
parseAddress = addresses => {
    if (typeof addresses == "string") {
        addresses = [addresses];
    } else if (typeof addresses == "object") {
        addresses = [...addresses];
    } else {
        addresses = null;
    }
    return addresses;
};
//-------------------------------------------------------------------------
// To parse url, set protocal and validate via regex
//-------------------------------------------------------------------------
parseUrl = url => {
    if (!url.includes("http")) {
        const regex = new RegExp(
            /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm
        );
        let result = regex.test(url);
        if (result) {
            url = `http://${url}`;
        } else {
            return false;
        }
    }
    return url;
};
//-------------------------------------------------------------------------
// To listen for requests on server
//-------------------------------------------------------------------------
app.listen(port, () => {
    console.log(`---> Server listening on port ${port}`);
});
//-------------------------------------------------------------------------
