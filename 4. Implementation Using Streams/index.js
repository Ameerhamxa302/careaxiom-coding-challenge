//-------------------------------------------------------------------------
/* Bonus: Implementation Using Streams */
//-------------------------------------------------------------------------
const express = require("express");
const cheerio = require("cheerio");
const axios = require("axios");
const fs = require("fs");
const app = express();
const port = 3000;
//-------------------------------------------------------------------------
// Setting the view engine to 'ejs'
//-------------------------------------------------------------------------
app.set("view engine", "ejs");
//-------------------------------------------------------------------------
// Handling required single 'GET' request route
//-------------------------------------------------------------------------
app.get("/I/want/title/:address?", (req, res) => {
    let titles = [];
    let addresses = parseAddress(req.query.address);

    if (addresses) {
        let counter = 0;
        addresses.forEach(url => {
            let parsedUrl = parseUrl(url);
            if (typeof parsedUrl == "string") {
                axios({
                    method: "get",
                    url: parsedUrl,
                    responseType: "stream"
                }).then(function(response) {
                    response.data.pipe(fs.createWriteStream("website.txt"));
                    fs.readFile("website.txt", "utf8", (err, data) => {
                        if (err) throw err;

                        const $ = cheerio.load(data);
                        titles.push({
                            address: url,
                            title: $("title").text()
                        });
                        counter++;

                        if (counter == addresses.length) {
                            res.render("index", {
                                titles: titles
                            });
                        }
                    });
                });
            } else {
                titles.push({
                    address: url,
                    title: "No Response"
                });
                counter++;

                if (counter == addresses.length) {
                    res.render("index", {
                        titles: titles
                    });
                }
            }
        });
    } else {
        res.render("index", {
            titles: titles
        });
    }
});
//-------------------------------------------------------------------------
// Handling all other routes
//-------------------------------------------------------------------------
app.get("*", (req, res) => {
    res.render("404");
});
//-------------------------------------------------------------------------
// To parse address from url based on type
//-------------------------------------------------------------------------
parseAddress = addresses => {
    if (typeof addresses == "string") {
        addresses = [addresses];
    } else if (typeof addresses == "object") {
        addresses = [...addresses];
    } else {
        addresses = null;
    }
    return addresses;
};
//-------------------------------------------------------------------------
// To parse url, set protocal and validate via regex
//-------------------------------------------------------------------------
parseUrl = url => {
    if (!url.includes("http")) {
        const regex = new RegExp(
            /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm
        );
        let result = regex.test(url);
        if (result) {
            url = `http://${url}`;
        } else {
            return false;
        }
    }
    return url;
};
//-------------------------------------------------------------------------
// To listen for requests on server
//-------------------------------------------------------------------------
app.listen(port, () => {
    console.log(`---> Server listening on port ${port}`);
});
//-------------------------------------------------------------------------
